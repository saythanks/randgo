<?php

namespace SayThanks\Randgo\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class FlushCacheCommand extends Command
{

    protected $signature = 'randgo:cache-flush';

    protected $description = 'Flush the randgo cache';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        Cache::tags(['randgo'])->flush();

        return self::SUCCESS;
    }
}
