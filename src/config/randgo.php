<?php

return

    /*
    |--------------------------------------------------------------------------
    | API Settings
    |--------------------------------------------------------------------------
    |
    |
    */
    [
        'host' => env('RANDGO_HOST', 'https://api.randgoqa.dev/api/'),
        'username' => env('RANDGO_USERNAME'),
        'password' => env('RANDGO_PASSWORD'),
        'client_scheme_guid' => env('RANDGO_CLIENT_SCHEME_GUID'),
        'client_scheme_member_identifier_guid' => env('RANDGO_CLIENT_SCHEME_MEMBER_IDENTIFIER_GUID'),
    ];