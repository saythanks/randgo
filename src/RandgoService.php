<?php

namespace SayThanks\Randgo;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use SayThanks\Randgo\Entities\AccountEntity;
use SayThanks\Randgo\Entities\GamificationEntity;
use SayThanks\Randgo\Entities\IncentivisationEntity;
use SayThanks\Randgo\Entities\MemberEntity;
use SayThanks\Randgo\Entities\UtilityEntity;
use SayThanks\Randgo\Entities\VoucherEntity;
use SayThanks\Randgo\Entities\WebHooksEntity;
use SayThanks\Tremendous\Exceptions\InvalidConfigException;

class RandgoService
{
    public ?string $host;
    public ?string $username;
    public ?string $client_scheme_guid;
    public ?string $client_scheme_member_identifier_guid;
    private ?string $password;
    public ?string $sessionToken = null;

    public string $name = 'RandGo';

    public const INVALID_SESSION_TOKEN_MESSAGE = 'Please Log in to obtain a valid Sessiontoken.';

    public function __construct()
    {
        $this->host = config('randgo.host');
        $this->username = config('randgo.username');
        $this->password = config('randgo.password');
        $this->client_scheme_guid = config('randgo.client_scheme_guid');
        $this->client_scheme_member_identifier_guid = config('randgo.client_scheme_member_identifier_guid');
        $this->validateConfig();
    }


    public function account()
    {
        return resolve(AccountEntity::class);
    }
    public function gamification()
    {
        return resolve(GamificationEntity::class);
    }
    public function incentivisation()
    {
        return resolve(IncentivisationEntity::class);
    }
    public function member()
    {
        return resolve(MemberEntity::class);
    }
    public function utility()
    {
        return resolve(UtilityEntity::class);
    }
    public function voucher()
    {
        return resolve(VoucherEntity::class);
    }
    public function webhook()
    {
        return resolve(WebHooksEntity::class);
    }

    /**
     * @throws InvalidConfigException
     */
    protected function validateConfig(): void
    {
        if(!$this->host)
        {
            throw new InvalidConfigException("Unable to find {$this->name} Host in your config!");
        }
        if(!$this->username)
        {
            throw new InvalidConfigException("Unable to find {$this->name} Username in your config!");
        }
        if(!$this->password)
        {
            throw new InvalidConfigException("Unable to find {$this->name} Password in your config!");
        }
        if(!$this->client_scheme_guid)
        {
            Log::warning('Unable to find {$this->name} client_scheme_guid in your config! See account()->getAccountDetails().');
        }
        if(!$this->client_scheme_member_identifier_guid)
        {
            Log::warning('Unable to find {$this->name} client_scheme_member_identifier_guid in your config! See account()->getAccountDetails().');
        }

    }


    public function apiCall() : PendingRequest
    {
        return Http::asJson()
            ->acceptJson();
    }

    public function authorize(bool $bustCache = false): ?string
    {
        if($bustCache)
        {
            Log::info($this->name . ': Busting Auth Cache!');
            Cache::tags(['randgo'])->forget('sessionToken');
        }

        $data = Cache::tags(['randgo'])->get('sessionToken');
        if($data !== null){
            $this->sessionToken = $data;
            return $this->sessionToken;
        }
        Log::info($this->name . ': Logging in', [
            'username' => $this->username,
            'url' => $this->host . '/Account/Login'
        ]);
        try{
            $loginData = $this->apiCall()->post($this->host . '/Account/Login',
                [
                    'username' => $this->username,
                    'password' => $this->password,
                ]);
            Log::debug($this->name . ': Login Response', [
                'username' => $this->username,
                'url' => $this->host . '/Account/Login',
                'response' => $loginData->object(),
            ]);
            if($loginData->object()->Success === true)
            {
                $this->sessionToken = $loginData->object()->Info->SessionToken;
                Cache::tags(['randgo'])->put('sessionToken', $this->sessionToken, 86340); //(1 day minute a minute)
                Log::info($this->name . ': Updated SessionToken Cache');
                return $this->sessionToken;
            }
        }
        catch(\Throwable $throwable)
        {
            Log::error($this->name . ': Failed To Login!', ['message' => $throwable->getMessage()]);
            throw $throwable;
        }
        Log::error($this->name . ': Failed To Login!', ['response-json' => $loginData->json()]);
        return null;
    }

    public function post(string $urlPart, array $data = [], int $retries = 1)
    {
        Log::info($this->name . ': Sending post', ['url' => $urlPart, 'data' => $data]);

        $this->authorize();

        $data['sessionToken'] = $this->sessionToken;
        try {
            $response = $this->apiCall()->post($this->host . $urlPart, $data);
            if($response->successful() && $response->object()?->Success === true)
            {
                Log::debug($this->name . ': Got response from post', ['url' => $urlPart, 'data' => $data, 'response-json' => $response->json()]);
            }
            else {

                if($response->object()->Message === static::INVALID_SESSION_TOKEN_MESSAGE)
                {
                    Log::error($this->name . ': Did a post which was apparently unauthorized. Forcefully reauthorizing.', ['old_token' => $this->sessionToken]);
                    $this->authorize(true);
                }
                if($retries > 0)
                {
                    Log::warning($this->name . ': Got invalid response from post, retrying', ['status' => $response->status(), 'url' => $urlPart, 'data' => $data, 'response-json' => $response->json(), 'retries' => $retries]);
                    return $this->post($urlPart, $data, ($retries-1));
                }
                Log::warning($this->name . ': Got invalid response from post', ['status' => $response->status(), 'url' => $urlPart, 'data' => $data, 'response-json' => $response->json()]);
            }
        }
        catch(\Throwable $throwable)
        {
            Log::error($this->name . ': Failed To Post!', ['request_data' => $data, 'url' => $urlPart, 'message' => $throwable->getMessage()]);
            throw $throwable;
        }

        return $response;
    }

}
