<?php
namespace SayThanks\Randgo\Facades;

use Illuminate\Support\Facades\Facade;

class Randgo extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'randgo';
    }
}