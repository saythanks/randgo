<?php

namespace SayThanks\Randgo;

use Illuminate\Support\ServiceProvider;
use SayHi\Switchfox\Console\Commands\FlushCacheCommand;

class RandgoServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/config/randgo.php', 'randgo');

        $this->app->singleton(RandgoService::class, function () {
            return new RandgoService();
        });

    }

    public function boot()
    {
        $this->publishes(
            [
                __DIR__ . '/config/randgo.php' => config_path('randgo.php'),
            ], 'config');

        $this->commands([
            FlushCacheCommand::class
        ]);
    }
}