<?php

namespace SayThanks\Randgo\Entities;


class UtilityEntity extends ApiEntity
{
    public string $name = 'Utility';
    public string $url = '/Utility';

    public function getResponse()
    {
        $url = $this->url . '/GetResponse';
        return $this->post($url);
    }
}
