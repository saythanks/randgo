<?php

namespace SayThanks\Randgo\Entities;


class AccountEntity extends ApiEntity
{
    public string $name = 'Account';
    public string $url = '/Account';

    public function getAccountDetails()
    {
        $url = $this->url . '/AccountDetailsGet';
        return $this->post($url);
    }

}
