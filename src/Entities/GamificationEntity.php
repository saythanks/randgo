<?php

namespace SayThanks\Randgo\Entities;


class GamificationEntity extends ApiEntity
{
    public string $name = 'Gamification';
    public string $url = '/Gamification';
}
