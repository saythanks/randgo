<?php

namespace SayThanks\Randgo\Entities;


class IncentivisationEntity extends ApiEntity
{
    public string $name = 'Incentivisation';
    public string $url = '/Incentivisation';
}
