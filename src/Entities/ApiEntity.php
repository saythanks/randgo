<?php

namespace SayThanks\Randgo\Entities;

use SayThanks\Randgo\RandgoService;

class ApiEntity
{
    public string $name;

    public function __construct()
    {
        $this->service = resolve(RandgoService::class);
    }

    protected function post(...$args)
    {
        return $this->service->post(...$args)->object();
    }

    protected function getClientSchemeGuid(): string
    {
        return $this->service->client_scheme_guid;
    }
    protected function getClientSchemeMemberIdentifierGuid(): string
    {
        return $this->service->client_scheme_member_identifier_guid;
    }
}
