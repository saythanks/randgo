<?php

namespace SayThanks\Randgo\Entities;

class MemberEntity extends ApiEntity
{
    public string $name = 'Member';
    public string $url = '/Member';

    public function list(bool $onlyActive = true, int $pageNumber = 0, string $clientSchemeGuid = null)
    {
        $clientSchemeGuid = $clientSchemeGuid ?? $this->getClientSchemeGuid();
        $url = $this->url . '/Export';
        return $this->post($url, ['ClientSchemeGuid' => $clientSchemeGuid, 'OnlyActive' => $onlyActive, 'PageNumber' => $pageNumber]);
    }

    public function find(string $primaryKeyName, string $primaryKeyValue)
    {
        $url = $this->url . '/Validate';
        return $this->post($url, ['PrimaryKeyName' => $primaryKeyName, 'PrimaryKeyValue' => $primaryKeyValue]);
    }

    public function create(
        string $primaryKeyName,
        string $uniqueUserKey,
        string $name,
        string $surname,
        string $cellphone,
        string $email,
        bool $active,
        string $username,
        string $clientSchemeGuid = null,
        string $clientSchemeMemberIdentifierGuid = null,)
    {
        $clientSchemeGuid = $clientSchemeGuid ?? $this->getClientSchemeGuid();
        $clientSchemeMemberIdentifierGuid = $clientSchemeMemberIdentifierGuid ?? $this->getClientSchemeMemberIdentifierGuid();
        $url = $this->url . '/Import';
        return $this->post($url, [
            'ClientSchemeGuid' => $clientSchemeGuid,
            'ClientSchemeMemberIdentifierGuid' => $clientSchemeMemberIdentifierGuid,
            'PrimaryKeyName' => $primaryKeyName,
            "Members" => [[
                "UniqueUserKey" => $uniqueUserKey,
                "Name" => $name,
                "Surname" => $surname,
                "Cellphone" => $cellphone,
                "Email" => $email,
                "Active" => $active,
                "Username" => $username,
            ]]]);
    }

    public function getMemberTypes()
    {
        $url = $this->url . '/MemberTypesGet';
        return $this->post($url);
    }

    public function getGenders()
    {
        $url = $this->url . '/GendersGet';
        return $this->post($url);
    }

    public function getPrimaryKey()
    {
        $url = $this->url . '/PrimaryKeyGet';
        return $this->post($url);
    }

    public function getEncryptedString(string $stringToEncrypt)
    {
        $url = $this->url . '/GetEncryptedString';
        return $this->post($url, ['StringToEncrypt' => $stringToEncrypt]);
    }

    public function getBatchAll()
    {
        $url = $this->url . '/Import/Batch/GetAll';
        return $this->post($url);
    }

    public function getBatchByGuid(string $guid)
    {
        $url = $this->url . '/Import/Batch/GetByBatchGuid';
        return $this->post($url, ['BatchGuid' => $guid]);
    }
}
