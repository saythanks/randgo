<?php

namespace SayThanks\Randgo\Entities;


class WebHooksEntity extends ApiEntity
{
    public string $name = 'Webhooks';
    public string $url = '/WebHooks';
}
