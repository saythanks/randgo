<?php

namespace SayThanks\Randgo\Entities;

use Carbon\Carbon;

class VoucherEntity extends ApiEntity
{
    public string $name = 'Voucher';
    public string $url = '/Voucher';

    public function getVoucherTypes()
    {
        $url = $this->url . '/VoucherTypesGet';
        return $this->post($url);
    }

    public function getVoucherRedemptionTypes()
    {
        $url = $this->url . '/VoucherRedemptionTypesGet';
        return $this->post($url);
    }

    public function list()
    {
        $url = $this->url . '/VouchersGet';
        return $this->post($url);
    }

    public function history(Carbon $dateFrom, Carbon $dateTo)
    {
        $url = $this->url . '/Issued';
        return $this->post($url, [
            'DateFrom' => $dateFrom->toIso8601ZuluString(),
            'DateTo' => $dateTo->toIso8601ZuluString(),
        ]);
    }

    public function issue(
        string $primaryKeyName,
        string $primaryKeyValue,
        string $alternativeCellphone,
        string $alternativeEmail,
        string $voucherGuid,
        string $redemptionType,
    )
    {
        $url = $this->url . '/IssueVoucher';
        $data = [
            "PrimaryKeyName" => $primaryKeyName,
            "PrimaryKeyValue" => $primaryKeyValue,
            "AlternativeCellphone" => $alternativeCellphone,
            "AlternativeEmail" => $alternativeEmail,
            "Vouchers" => [
                [
                    "VoucherGuid" => $voucherGuid,
                    "RedemptionType" => $redemptionType,
                ]
            ]
        ];
        return $this->post($url, $data);
    }
}
